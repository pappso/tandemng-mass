/*
 Copyright (C) 2003-2004 Ronald C Beavis, all rights reserved
 X! tandem
 This software is a component of the X! proteomics software
 development project

Use of this software governed by the Artistic license, as reproduced here:

The Artistic License for all X! software, binaries and documentation

Preamble
The intent of this document is to state the conditions under which a
Package may be copied, such that the Copyright Holder maintains some
semblance of artistic control over the development of the package,
while giving the users of the package the right to use and distribute
the Package in a more-or-less customary fashion, plus the right to
make reasonable modifications.

Definitions
"Package" refers to the collection of files distributed by the Copyright
  Holder, and derivatives of that collection of files created through
  textual modification.

"Standard Version" refers to such a Package if it has not been modified,
  or has been modified in accordance with the wishes of the Copyright
  Holder as specified below.

"Copyright Holder" is whoever is named in the copyright or copyrights
  for the package.

"You" is you, if you're thinking about copying or distributing this Package.

"Reasonable copying fee" is whatever you can justify on the basis of
  media cost, duplication charges, time of people involved, and so on.
  (You will not be required to justify it to the Copyright Holder, but
  only to the computing community at large as a market that must bear
  the fee.)

"Freely Available" means that no fee is charged for the item itself,
  though there may be fees involved in handling the item. It also means
  that recipients of the item may redistribute it under the same
  conditions they received it.

1. You may make and give away verbatim copies of the source form of the
Standard Version of this Package without restriction, provided that
you duplicate all of the original copyright notices and associated
disclaimers.

2. You may apply bug fixes, portability fixes and other modifications
derived from the Public Domain or from the Copyright Holder. A
Package modified in such a way shall still be considered the Standard
Version.

3. You may otherwise modify your copy of this Package in any way, provided
that you insert a prominent notice in each changed file stating how and
when you changed that file, and provided that you do at least ONE of the
following:

a.	place your modifications in the Public Domain or otherwise make them
  Freely Available, such as by posting said modifications to Usenet
  or an equivalent medium, or placing the modifications on a major
  archive site such as uunet.uu.net, or by allowing the Copyright Holder
  to include your modifications in the Standard Version of the Package.
b.	use the modified Package only within your corporation or organization.
c.	rename any non-standard executables so the names do not conflict
  with standard executables, which must also be provided, and provide
  a separate manual page for each non-standard executable that clearly
  documents how it differs from the Standard Version.
d.	make other distribution arrangements with the Copyright Holder.

4. You may distribute the programs of this Package in object code or
executable form, provided that you do at least ONE of the following:

a.	distribute a Standard Version of the executables and library files,
  together with instructions (in the manual page or equivalent) on
  where to get the Standard Version.
b.	accompany the distribution with the machine-readable source of the
  Package with your modifications.
c.	give non-standard executables non-standard names, and clearly
  document the differences in manual pages (or equivalent), together
  with instructions on where to get the Standard Version.
d.	make other distribution arrangements with the Copyright Holder.

5. You may charge a reasonable copying fee for any distribution of
this Package. You may charge any fee you choose for support of
this Package. You may not charge a fee for this Package itself.
However, you may distribute this Package in aggregate with other
(possibly commercial) programs as part of a larger (possibly
commercial) software distribution provided that you do not a
dvertise this Package as a product of your own. You may embed this
Package's interpreter within an executable of yours (by linking);
this shall be construed as a mere form of aggregation, provided that
the complete Standard Version of the interpreter is so embedded.

6. The scripts and library files supplied as input to or produced as
output from the programs of this Package do not automatically fall
under the copyright of this Package, but belong to whomever generated
them, and may be sold commercially, and may be aggregated with this
Package. If such scripts or library files are aggregated with this
Package via the so-called "undump" or "unexec" methods of producing
a binary executable image, then distribution of such an image shall
neither be construed as a distribution of this Package nor shall it
fall under the restrictions of Paragraphs 3 and 4, provided that you
do not represent such an executable image as a Standard Version of
this Package.

7. C subroutines (or comparably compiled subroutines in other languages)
supplied by you and linked into this Package in order to emulate
subroutines and variables of the language defined by this Package
shall not be considered part of this Package, but are the equivalent
of input as in Paragraph 6, provided these subroutines do not change
the language in any way that would cause it to fail the regression
tests for the language.

8. Aggregation of this Package with a commercial distribution is always
permitted provided that the use of this Package is embedded; that is,
when no overt attempt is made to make this Package's interfaces visible
to the end user of the commercial distribution. Such use shall not be
construed as a distribution of this Package.

9. The name of the Copyright Holder may not be used to endorse or promote
products derived from this software without specific prior written permission.

10. THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

The End
*/

// File version: 2004-02-01
// File version: 2005-01-01
/*
 * the mreport object provides the functionality to export all of the
 * information collected in mprocess to an XML file. The file path was defined
 * in the original XML input parameter file. The file produced by mreport can be
 * used as an input parameter file to repeat a protein modeling session at a
 * later time.
 */
#include <sys/timeb.h>
#include <ctime>
#include <cstdint>
#include <pappsomspp/pappsoexception.h>
#include "xmlparameter.h"
#include "msequtilities.h"
#include "stdafx.h"

//#include "saxsaphandler.h"

#ifdef HEADER2017
#include "common.h"
#include "msequence.h"
#include "msequencecollection.h"
#include "msequenceserver.h"
#include "mspectrum.h"
#include "mscore.h"
#include "mprocess.h"
const QString mreport::m_processFlavor = "2017";
#endif


#ifdef HEADER2015
#include "../common.h"
#include "../msequence.h"
#include "2015/msequencecollection.h"
#include "2015/msequenceserver.h"
#include "2015/mspectrum.h"
#include "2015/mscore.h"
#include "2015/mprocess.h"
const QString mreport::m_processFlavor = "2015";
#endif


#ifdef HEADER2015P
#include "../common.h"
#include "../msequence.h"
#include "2015p/msequencecollection.h"
#include "2015p/msequenceserver.h"
#include "msequtilities.h"
#include "2015p/mspectrum.h"
#include "2015p/mscore.h"
#include "2015p/mprocess.h"
const QString mreport::m_processFlavor = "2015p";
#endif


#include "mreport.h"


const QString mreport::m_xmlnsGaml = "http://www.bioml.com/gaml/";

mreport::mreport(mscore &score) : m_Score(score)
{
  m_lHistogramColumns = 30;
  m_bCompress         = false;
}

mreport::~mreport(void)
{
}
/*
 * Sets the value for the number of values in a GAML value list before a return
 */
bool
mreport::set_columns(const long _v)
{
  if(_v < 1)
    return false;
  m_lHistogramColumns = _v;
  return true;
}
/*
 * Sets the value for the m_bCompress flag. When set, only one copy of a
 * protein's sequence is stored in the output XML file.
 */
bool
mreport::set_compression(const bool _b)
{
  m_bCompress = _b;
  return m_bCompress;
}
/*
 * Checks a protein uid to see if it has already been used, storing the uid in
 * m_vtProteins if it has not been stored. This function is used to reduce the
 * number of protein sequences stored in an XML file
 */
bool
mreport::check_proteins(const size_t _t)
{
  if(!m_bCompress)
    return true;
  if(m_setProteins.find(_t) == m_setProteins.end())
    {
      m_setProteins.insert(_t);
      return true;
    }
  return false;
}
/*
 * end finishes the XML document
 */
bool
mreport::end(void)
{
  m_outputStream->writeEndDocument();
  m_outputFile->close();


  delete m_outputFile;
  delete m_outputStream;

  return true;
}
/*
 * endgroup finishes the group object associated with a particular mspectrum
 * object
 */
bool
mreport::endProteinGroup() const
{
  m_outputStream->writeEndElement(); // "</group>\n";
  return true;
}
/*
 * group starts the group associated with outputing the information from a
 * particular mspectrum object
 */
bool
mreport::startProteinGroup(const mspectrum &_s) const
{
  size_t tId = _s.m_tId;
  while(tId > 100000000)
    {
      tId -= 100000000;
    }

  m_outputStream->writeStartElement("group");
  m_outputStream->writeAttribute("id", QString::number(tId));
  m_outputStream->writeAttribute("mh", QString::number(_s.m_dMH, 'f', 10));
  m_outputStream->writeAttribute("z", QString::number(_s.m_fZ));
  m_outputStream->writeAttribute(
    "rt",
    QString("PT%1S").arg(QString::number(_s.m_retentionTimeInSeconds, 'f', 4)));
  m_outputStream->writeAttribute(
    "sumI", QString::number(log10(_s.m_vdStats[0]), 'f', 2));
  m_outputStream->writeAttribute("maxI", QString::number(_s.m_vdStats[1]));
  m_outputStream->writeAttribute("fI", QString::number(_s.m_vdStats[2]));
  m_outputStream->writeAttribute("act", QString::number(_s.m_uiType));
  m_outputStream->writeAttribute("type", "model");
  if(_s.m_vseqBest.size() == 0)
    {
      // m_ofOut << "<group id=\"" << (unsigned long)tId << "\" ";
      // sprintf(pLine, "%.6lf", _s.m_dMH);
      // m_ofOut << "mh=\"" << pLine << "\" ";
      // m_ofOut << "z=\"" << (long)_s.m_fZ << "\" ";
      // m_ofOut << "rt=\"" << _s.m_strRt.c_str() << "\" ";
      m_outputStream->writeAttribute("expect", "1000");
      // m_ofOut << "expect=\"1000\" ";:
      m_outputStream->writeAttribute("label", "no model obtained");
      // m_outputStream->writeAttribute("type", "model");
      // sprintf(pLine, "%.2lf", log10(_s.m_vdStats[0]));
      /*m_ofOut << "label=\""
              << "no model obtained"
              << "\" type=\"model\" ";
      m_ofOut << "sumI=\"" << pLine << "\" maxI=\"" << _s.m_vdStats[1]
              << "\" fI=\"" << _s.m_vdStats[2] << "\" ";
      m_ofOut << "act=\"" << _s.m_uiType << "\" ";
    */
    }
  else
    {
      // m_ofOut << "<group id=\"" << (unsigned long)tId << "\" ";
      // sprintf(pLine, "%.6lf", _s.m_dMH);
      // m_ofOut << "mh=\"" << pLine << "\" ";
      // m_ofOut << "z=\"" << (long)_s.m_fZ << "\" ";
      // m_ofOut << "rt=\"" << _s.m_strRt.c_str() << "\" ";

      m_outputStream->writeAttribute("expect", evalueStr(_s.m_dExpect));
      // sprintf(pLine, "%.1e", _s.m_dExpect);
      // m_ofOut << "expect=\"" << pLine << "\" ";

      m_outputStream->writeAttribute(
        "label", label80Str(_s.m_vseqBest[0].m_strDes.c_str()));
      // string strV = _s.m_vseqBest[0].m_strDes;
      // format_text(strV);
      // get_short_label(strV, pLine, 80, 255);
      // m_ofOut << "label=\"" << pLine << "\" type=\"model\" ";
      // sprintf(pLine, "%.2lf", log10(_s.m_vdStats[0]));
      // m_ofOut << "sumI=\"" << pLine << "\" maxI=\"" << _s.m_vdStats[1]
      //       << "\" fI=\"" << _s.m_vdStats[2] << "\" ";
      // m_ofOut << "act=\"" << _s.m_uiType << "\" ";
      //  m_ofOut << ">\n";
    }
  // m_ofOut << " >\n";
  // delete pLine;
  return true;
}

void
mreport::writeStartGamlTrace(std::size_t xml_id,
                             const QString &label,
                             const QString &type) const
{

  m_outputStream->writeStartElement(m_xmlnsGaml, "trace");
  m_outputStream->writeAttribute("label",
                                 QString("%1.%2").arg(xml_id).arg(label));
  m_outputStream->writeAttribute("type", type);


  // m_outputStream->writeEndElement();
}

void
mreport::writeGamlValues(const QString &data_str,
                         std::size_t xml_id,
                         const QString &label,
                         const QString &units,
                         const std::vector<double> &list_values) const
{

  m_outputStream->writeStartElement(m_xmlnsGaml, data_str);
  m_outputStream->writeAttribute("label",
                                 QString("%1.%2").arg(xml_id).arg(label));
  m_outputStream->writeAttribute("units", units);
  // m_ofOut << "<GAML:Ydata label=\"" << tId
  //       << ".convolute\" units=\"counts\">\n";

  m_outputStream->writeStartElement(m_xmlnsGaml, "values");
  m_outputStream->writeAttribute("byteorder", "INTEL");
  m_outputStream->writeAttribute("format", "ASCII");
  m_outputStream->writeAttribute("numvalues",
                                 QString::number(list_values.size()));
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << lLength << "\">\n";
  QStringList list_a;
  for(auto &value : list_values)
    {
      if(units == "UNKNOWN")
        {
          list_a << QString::number(value, 'f', 0);
        }
      else
        {
          list_a << QString::number(value);
        }
    }
  m_outputStream->writeCharacters(list_a.join(" "));

  m_outputStream->writeEndElement(); //"\n</GAML:values>
  m_outputStream->writeEndElement(); //"\n</GAML:Ydata>
}


/*
 * histogram outputs all of the histrograms contained in an mspectrum object.
 * GAML notation is used to output the histograms
 */
bool
mreport::histogram(mspectrum &_s)
{
  std::int64_t tId = _s.m_tId;
  while(tId > 100000000)
    {
      tId -= 100000000;
    }
  m_outputStream->writeStartElement("group");
  m_outputStream->writeAttribute("label", "supporting data");
  m_outputStream->writeAttribute("type", "support");
  // m_ofOut << "<group label=\"supporting data\" type=\"support\">\n";

  writeStartGamlTrace(tId, "hyper", "hyperscore expectation function");
  // m_ofOut << "<GAML:trace label=\"" << tId
  //         << ".hyper\" type=\"hyperscore expectation function\">\n";


  m_outputStream->writeStartElement(m_xmlnsGaml, "attribute");
  m_outputStream->writeAttribute("type", "a0");
  m_outputStream->writeCharacters(QString::number(_s.m_hHyper.a0()));
  // m_ofOut << "<GAML:attribute type=\"a0\">" << _s.m_hHyper.a0()
  //         << "</GAML:attribute>\n";
  m_outputStream->writeEndElement(); //("GAML:attribute");
  m_outputStream->writeStartElement(m_xmlnsGaml, "attribute");
  m_outputStream->writeAttribute("type", "a1");
  m_outputStream->writeCharacters(QString::number(_s.m_hHyper.a1()));
  // m_ofOut << "<GAML:attribute type=\"a1\">" << _s.m_hHyper.a1()
  //       << "</GAML:attribute>\n";
  m_outputStream->writeEndElement(); //("GAML:attribute");


  std::vector<double> list_values;
  // m_ofOut << "<GAML:Xdata label=\"" << tId << ".hyper\" units=\"score\">\n";
  long a = _s.m_hHyper.length() - 1;
  _s.m_hHyper.survival();
  while(a >= 0 && _s.m_hHyper.survive(a) < 1)
    {
      a--;
    }
  if(a == _s.m_hHyper.length() - 1)
    a = _s.m_hHyper.length() - 2;
  long lLength = a + 2;
  long lCount  = 0;
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //        << lLength << "\">\n";
  a = 0;
  while(a < lLength)
    {
      list_values.push_back(a);
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }
  writeGamlValues("Xdata", tId, "hyper", "score", list_values);
  // m_ofOut << "\n</GAML:values>\n</GAML:Xdata>\n";


  // m_ofOut << "<GAML:Ydata label=\"" << tId << ".hyper\" units=\"counts\">\n";
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << lLength << "\">\n";
  list_values.clear();
  a      = 0;
  lCount = 0;
  while(a < lLength)
    {

      list_values.push_back(_s.m_hHyper.survive(a));
      // m_ofOut << _s.m_hHyper.survive(a);
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }
  _s.m_hHyper.clear_survive();

  writeGamlValues("Ydata", tId, "hyper", "counts", list_values);


  m_outputStream->writeEndElement(); //"\n</GAML:trace>
  // m_ofOut << "\n</GAML:values>\n</GAML:Ydata>\n</GAML:trace>\n";


  writeStartGamlTrace(tId, "convolute", "convolution survival function");
  // m_ofOut << "<GAML:trace label=\"" << tId
  //       << ".convolute\" type=\"convolution survival function\">\n";


  // m_ofOut << "<GAML:Xdata label=\"" << tId << ".convolute\"
  // units=\"score\">\n";
  a = _s.m_hConvolute.length() - 1;
  _s.m_hConvolute.survival();
  while(a >= 0 && _s.m_hConvolute.survive(a) < 1)
    {
      a--;
    }
  if(a == _s.m_hConvolute.length() - 1)
    a = _s.m_hConvolute.length() - 2;
  lLength = a + 2;

  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << lLength << "\">\n";

  list_values.clear();
  a      = 0;
  lCount = 0;
  while(a < lLength)
    {
      list_values.push_back(a);
      // m_ofOut << a;
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }
  writeGamlValues("Xdata", tId, "convolute", "score", list_values);
  // m_ofOut << "\n</GAML:values>\n</GAML:Xdata>\n";


  // m_ofOut << "<GAML:Ydata label=\"" << tId
  //       << ".convolute\" units=\"counts\">\n";
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << lLength << "\">\n";
  a      = 0;
  lCount = 0;
  list_values.clear();
  while(a < lLength)
    {
      list_values.push_back(_s.m_hConvolute.survive(a));
      // m_ofOut << _s.m_hConvolute.survive(a);
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }
  _s.m_hConvolute.clear_survive();

  writeGamlValues("Ydata", tId, "convolute", "count", list_values);
  m_outputStream->writeEndElement(); //"\n</GAML:trace>
  // m_ofOut << "\n</GAML:values>\n</GAML:Ydata>\n</GAML:trace>\n";


  writeStartGamlTrace(tId, "b", "b ion histogram");
  // m_ofOut << "<GAML:trace label=\"" << tId
  //       << ".b\" type=\"b ion histogram\">\n";
  // m_ofOut << "<GAML:Xdata label=\"" << tId
  //      << ".b\" units=\"number of ions\">\n";
  a = _s.m_chBCount.length() - 1;
  while(a >= 0 && _s.m_chBCount.list(a) < 1)
    {
      a--;
    }
  if(a == _s.m_chBCount.length() - 1)
    a = _s.m_chBCount.length() - 2;
  lLength = a + 2;
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << lLength << "\">\n";
  a      = 0;
  lCount = 0;
  list_values.clear();
  while(a < lLength)
    {
      // m_ofOut << a;
      list_values.push_back(a);
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }

  writeGamlValues("Xdata", tId, "b", "number of ions", list_values);
  // m_ofOut << "\n</GAML:values>\n</GAML:Xdata>\n";

  // m_ofOut << "<GAML:Ydata label=\"" << tId << ".b\" units=\"counts\">\n";
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << lLength << "\">\n";
  a      = 0;
  lCount = 0;
  list_values.clear();
  while(a < lLength)
    {
      list_values.push_back(_s.m_chBCount.list(a));
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }

  writeGamlValues("Ydata", tId, "b", "counts", list_values);
  m_outputStream->writeEndElement(); //</GAML:trace>
  // m_ofOut << "\n</GAML:values>\n</GAML:Ydata>\n</GAML:trace>\n";


  writeStartGamlTrace(tId, "y", "y ion histogram");
  // m_ofOut << "<GAML:trace label=\"" << tId
  //       << ".y\" type=\"y ion histogram\">\n";
  // m_ofOut << "<GAML:Xdata label=\"" << tId
  //       << ".y\" units=\"number of ions\">\n";
  a = _s.m_chYCount.length() - 1;
  while(a >= 0 && _s.m_chYCount.list(a) < 1)
    {
      a--;
    }
  if(a == _s.m_chYCount.length() - 1)
    a = _s.m_chYCount.length() - 2;
  lLength = a + 2;
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << lLength << "\">\n";
  a      = 0;
  lCount = 0;
  list_values.clear();
  while(a < lLength)
    {
      list_values.push_back(a);
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }

  writeGamlValues("Xdata", tId, "y", "number of ions", list_values);
  // m_ofOut << "\n</GAML:values>\n</GAML:Xdata>\n";
  // m_ofOut << "<GAML:Ydata label=\"" << tId << ".y\" units=\"counts\">\n";
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //      << lLength << "\">\n";
  a      = 0;
  lCount = 0;
  list_values.clear();
  while(a < lLength)
    {
      list_values.push_back(_s.m_chYCount.list(a));
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          //  m_ofOut << " ";
        }
      a++;
    }

  writeGamlValues("Ydata", tId, "y", "counts", list_values);
  // m_ofOut << "\n</GAML:values>\n</GAML:Ydata>\n</GAML:trace>\n";

  m_outputStream->writeEndElement(); //</GAML:trace>
  m_outputStream->writeEndElement(); // group

  return true;
}
/*
 * info outputs the input parameters derived from the input file and the default
 * input file (if used). This record should be sufficient to repeat this protein
 * modeling session exactly, without reference to the original input files. Two
 * group nodes are used to record this information. The "input parameters" group
 * contains all of the input nodes that were used to create this output file.
 * The "unused input parameters" group contains all of the input parameters that
 * were either unsupported, mis-entered or otherwise ignored.
 */

bool
mreport::info(XmlParameter &_x)
{
  pair<string, string> pairValue;
  xMap::iterator itStart = _x.m_mapParam.begin();
  xMap::iterator itEnd   = _x.m_mapParam.end();
  size_t a               = 0;
  string strValue;
  m_outputStream->writeStartElement("group");
  m_outputStream->writeAttribute("label", "input parameters");
  m_outputStream->writeAttribute("type", "parameters");
  // m_ofOut << "<group label=\"input parameters\" type=\"parameters\">\n";
  size_t tUnused = 0;
  while(itStart != itEnd)
    {
      pairValue = *itStart;
      if(_x.m_mapUsed[pairValue.first])
        {

          m_outputStream->writeStartElement("note");
          m_outputStream->writeAttribute("type", "input");
          m_outputStream->writeAttribute("label", pairValue.first.c_str());
          m_outputStream->writeCharacters(pairValue.second.c_str());
          m_outputStream->writeEndElement(); // note
          //  m_ofOut << "</note>\n";
        }
      else
        {
          tUnused++;
        }
      itStart++;
    }
  m_outputStream->writeEndElement(); // group
  // m_ofOut << "</group>\n";
  if(tUnused == 0)
    return true;
  itStart = _x.m_mapParam.begin();
  itEnd   = _x.m_mapParam.end();
  a       = 0;
  m_outputStream->writeStartElement("group");
  m_outputStream->writeAttribute("label", "unused input parameters");
  m_outputStream->writeAttribute("type", "parameters");
  // m_ofOut << "<group label=\"unused input parameters\"
  // type=\"parameters\">\n";
  while(itStart != itEnd)
    {
      pairValue = *itStart;
      if(!_x.m_mapUsed[pairValue.first])
        {
          m_outputStream->writeStartElement("note");
          m_outputStream->writeAttribute("type", "input");
          m_outputStream->writeAttribute("label", pairValue.first.c_str());
          m_outputStream->writeCharacters(pairValue.second.c_str());
          m_outputStream->writeEndElement(); // note
        }
      itStart++;
    }
  m_outputStream->writeEndElement(); // group
  // m_ofOut << "</group>\n";
  return true;
}
/*
 * performance is called to output a group node that contains any
 * performance parameters from the protein modeling session that might be of
 * interest after the protein modeling session. Any number of additional output
 * parameters can be included here. An XmlParameter object is used to supply the
 * information required to create a series of note objects that contain the
 * informtion.
 */
bool
mreport::performance(XmlParameter &_x)
{
  pair<string, string> pairValue;
  xMap::iterator itStart = _x.m_mapParam.begin();
  xMap::iterator itEnd   = _x.m_mapParam.end();
  size_t a               = 0;
  /*
   * create the group object
   */
  string strValue;
  m_outputStream->writeStartElement("group");
  m_outputStream->writeAttribute("label", "performance parameters");
  m_outputStream->writeAttribute("type", "parameters");
  // m_ofOut << "<group label=\"performance parameters\"
  // type=\"parameters\">\n";
  while(itStart != itEnd)
    {
      pairValue = *itStart;
      strValue  = pairValue.second;
      /*
       * create the individual notes
       */
      m_outputStream->writeStartElement("note");
      m_outputStream->writeAttribute("label", pairValue.first.c_str());
      m_outputStream->writeCharacters(pairValue.second.c_str());
      m_outputStream->writeEndElement(); // note
      // m_ofOut << "\t<note label=\"" << pairValue.first << "\">";
      // m_ofOut << "</note>\n";
      itStart++;
    }


  m_outputStream->writeStartElement("note");
  m_outputStream->writeAttribute("label", "process, tandemng");
  m_outputStream->writeCharacters(
    QString("tandemng %1 %2").arg(m_processFlavor).arg(tandemng_VERSION));
  m_outputStream->writeEndElement(); // note
  /*
   * finish the group
   */
  m_outputStream->writeEndElement(); // group
  // m_ofOut << "</group>\n";
  return true;
}
/*
 * write masses to the results file if residue masses are not default values
 */
bool
mreport::masses(msequtilities &_p)
{
  if(!_p.is_modified())
    {
      return false;
    }
  size_t a = 0;
  char cAa = 'A';
  /*
   * create the group object
   */
  // string strValue;
  // char *pLine = new char[256];
  m_outputStream->writeStartElement("group");
  m_outputStream->writeAttribute("label", "residue mass parameters");
  m_outputStream->writeAttribute("type", "parameters");
  // m_ofOut << "<group label=\"residue mass parameters\"
  // type=\"parameters\">\n";
  while(cAa <= 'Z')
    {
      m_outputStream->writeStartElement("aa");
      m_outputStream->writeAttribute("type", QString(cAa));
      m_outputStream->writeAttribute(
        "mass", QString::number(_p.m_pdAaMass[cAa], 'f', 10));

      m_outputStream->writeEndElement(); // aa
      cAa++;
    }
  m_outputStream->writeStartElement("molecule");
  m_outputStream->writeAttribute("type", "NH3");
  m_outputStream->writeAttribute("mass",
                                 QString::number(_p.m_dAmmonia, 'f', 10));
  // sprintf(pLine, "\t<molecule type=\"NH3\" mass=\"%.6lf\" />\n",
  // _p.m_dAmmonia); m_ofOut << pLine;

  m_outputStream->writeStartElement("molecule");
  m_outputStream->writeAttribute("type", "H2O");
  m_outputStream->writeAttribute("mass", QString::number(_p.m_dWater, 'f', 10));
  // sprintf(pLine, "\t<molecule type=\"H2O\" mass=\"%.6lf\" />\n",
  // _p.m_dWater); m_ofOut << pLine;
  /*
   * finish the group
   */
  m_outputStream->writeEndElement(); // group
  // m_ofOut << "</group>\n";
  // delete pLine;
  return true;
}
/*
 * spectrum is used to output a complete spectrum, using GAML notation.
 */
bool
mreport::spectrum(mspectrum &_s, string &_f)
{
  std::int64_t tId = _s.m_tId;
  while(tId > 100000000)
    {
      tId -= 100000000;
    }
  m_outputStream->writeStartElement("group");
  m_outputStream->writeAttribute("type", "support");
  m_outputStream->writeAttribute("label", "fragment ion mass spectrum");
  // m_ofOut << "<group type=\"support\" label=\"fragment ion mass
  // spectrum\">\n";
  if(!_f.empty())
    {
      m_outputStream->writeStartElement("file");
      m_outputStream->writeAttribute("type", "spectra");
      m_outputStream->writeAttribute("URL", _f.c_str());
      m_outputStream->writeEndElement();
      // m_ofOut << "<file type=\"spectra\" URL=\"" << _f.c_str() << "\" />\n";
    }
  if(!_s.m_strDescription.empty())
    {
      //_s.format();
      m_outputStream->writeStartElement("note");
      m_outputStream->writeAttribute("label", "Description");
      m_outputStream->writeCharacters(_s.m_strDescription.c_str());
      m_outputStream->writeEndElement();
      // m_ofOut << "<note label=\"Description\">" <<
      // _s.m_strDescription.c_str()
      //       << "</note>\n";
    }
  writeStartGamlTrace(tId, "spectrum", "tandem mass spectrum");
  // m_ofOut << "<GAML:trace id=\"" << tId << "\" label=\"" << tId
  //       << ".spectrum\" type=\"tandem mass spectrum\">\n";

  m_outputStream->writeStartElement(m_xmlnsGaml, "attribute");
  m_outputStream->writeAttribute("type", "M+H");
  m_outputStream->writeCharacters(massStr(_s.m_dMH));
  m_outputStream->writeEndElement();
  // m_ofOut << "<GAML:attribute type=\"M+H\">" << _s.m_dMH
  //        << "</GAML:attribute>\n";
  m_outputStream->writeStartElement(m_xmlnsGaml, "attribute");
  m_outputStream->writeAttribute("type", "charge");
  m_outputStream->writeCharacters(QString::number(_s.m_fZ));
  m_outputStream->writeEndElement();
  // m_ofOut << "<GAML:attribute type=\"charge\">" << _s.m_fZ
  //       << "</GAML:attribute>\n";
  // m_ofOut << "<GAML:Xdata label=\"" << tId
  //       << ".spectrum\" units=\"MASSTOCHARGERATIO\">\n";
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << (unsigned long)_s.m_vMI.size() << "\">\n";


  size_t a             = 0;
  const size_t tLength = _s.m_vMI.size();
  long lCount          = 0;
  std::vector<double> list_values;
  while(a < tLength)
    {
      list_values.push_back(_s.m_vMI[a].m_fM);
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }
  writeGamlValues("Xdata", tId, "spectrum", "MASSTOCHARGERATIO", list_values);
  // m_ofOut << "\n</GAML:values>\n</GAML:Xdata>\n";
  // m_ofOut << "<GAML:Ydata label=\"" << tId
  //       << ".spectrum\" units=\"UNKNOWN\">\n";
  // m_ofOut << "<GAML:values byteorder=\"INTEL\" format=\"ASCII\" numvalues=\""
  //       << (unsigned long)_s.m_vMI.size() << "\">\n";
  a      = 0;
  lCount = 0;
  list_values.clear();
  // char *pLine = new char[256];
  while(a < tLength)
    {
      list_values.push_back(_s.m_vMI[a].m_fI);
      // sprintf(pLine, "%.0f", _s.m_vMI[a].m_fI);
      // m_ofOut << pLine;
      lCount++;
      if(lCount == m_lHistogramColumns)
        {
          // m_ofOut << "\n";
          lCount = 0;
        }
      else
        {
          // m_ofOut << " ";
        }
      a++;
    }

  writeGamlValues("Ydata", tId, "spectrum", "UNKNOWN", list_values);

  m_outputStream->writeEndElement(); // trace

  m_outputStream->writeEndElement(); // group
  // m_ofOut << "\n</GAML:values>\n</GAML:Ydata>\n</GAML:trace>\n</group>";
  // delete pLine;
  return true;
}
/*
 * sequence outputs the information about an identified protein sequence, using
 * bioml notation
 */
bool
mreport::sequence(mspectrum &_s, vector<string> &_p, map<string, string> &_ann)
{
  size_t a         = 0;
  size_t b         = 0;
  size_t c         = 0;
  std::int64_t tId = _s.m_tId;
  while(tId > 100000000)
    {
      tId -= 100000000;
    }
  string strTemp;
  map<string, string>::iterator itMod;
  while(a < _s.m_vseqBest.size())
    {

      m_outputStream->writeStartElement("protein");
      m_outputStream->writeAttribute("expect",
                                     evalueStr(_s.m_vseqBest[a].m_dExpect));
      // m_ofOut << "<protein expect=\"";
      // sprintf(pLine, "%.1lf", _s.m_vseqBest[a].m_dExpect);
      // if(strstr(pLine, "-1.$"))
      //{
      // strcpy(pLine, "-5999.0");
      //}
      // m_ofOut << pLine << "\"";
      m_outputStream->writeAttribute(
        "id", QString("%1.%2").arg(tId).arg((unsigned long)(a + 1)));
      // m_ofOut << " id=\"" << tId << "." << (unsigned long)(a + 1) << "\"";
      m_outputStream->writeAttribute(
        "uid", QString::number((unsigned long)_s.m_vseqBest[a].m_tUid));
      // m_ofOut << " uid=\"" << (unsigned long)_s.m_vseqBest[a].m_tUid << "\"
      // ";
      m_outputStream->writeAttribute(
        "label", label80Str(_s.m_vseqBest[a].m_strDes.c_str()));
      // string strV = _s.m_vseqBest[a].m_strDes;
      // format_text(strV);
      // get_short_label(strV, pLine, 80, 250);
      // m_ofOut << "label=\"" << pLine << "\" ";
      m_outputStream->writeAttribute(
        "sumI", QString::number(log10(_s.m_vseqBest[a].m_fIntensity)));
      // sprintf(pLine, "sumI=\"%.2lf\" ",
      // log10(_s.m_vseqBest[a].m_fIntensity)); m_ofOut << pLine;
      itMod = _ann.find(_s.m_vseqBest[a].m_strDes);
      if(itMod != _ann.end())
        {

          m_outputStream->writeAttribute("annotation", (itMod->second).c_str());
          // sprintf(pLine, " annotation=\"%s\"", (itMod->second).c_str());
          // m_ofOut << pLine;
        }
      // m_ofOut << ">\n";

      m_outputStream->writeStartElement("note");
      m_outputStream->writeAttribute("label", "description");
      // m_ofOut << "<note label=\"description\">";
      m_outputStream->writeCharacters(_s.m_vseqBest[a].m_strDes.c_str());
      m_outputStream->writeEndElement();
      // m_ofOut << strV.c_str() << "</note>\n";
      m_outputStream->writeStartElement("file");
      m_outputStream->writeAttribute("type", "peptide");
      m_outputStream->writeAttribute("URL",
                                     _p[_s.m_vseqBest[a].m_siPath].c_str());
      m_outputStream->writeEndElement();
      // m_ofOut << "<file type=\"peptide\" URL=\""
      //       << _p[_s.m_vseqBest[a].m_siPath] << "\"/>\n";

      m_outputStream->writeStartElement("peptide");
      m_outputStream->writeAttribute("start", "1");
      m_outputStream->writeAttribute(
        "end",
        QString::number((unsigned long)_s.m_vseqBest[a].m_strSeq.size()));
      // m_ofOut << "<peptide start=\"1\" end=\""
      //       << (unsigned long)_s.m_vseqBest[a].m_strSeq.size() << "\">\n";
      c = 0;
      // m_ofOut << "\t";
      if(m_maptUids.find(_s.m_vseqBest[a].m_tUid) == m_maptUids.end())
        {
          m_maptUids[_s.m_vseqBest[a].m_tUid] = true;
        }
      if(check_proteins(_s.m_vseqBest[a].m_tUid))
        {
          m_outputStream->writeCharacters(_s.m_vseqBest[a].m_strSeq.c_str());
          /*
          while(_b && c < _s.m_vseqBest[a].m_strSeq.size())
            {

              m_ofOut << _s.m_vseqBest[a].m_strSeq[c];
              tRow++;
              tSpace++;
              if(tRow == 50)
                {
                  tSpace = 0;
                  tRow   = 0;
                  m_ofOut << "\n\t";
                }
              else if(tSpace == 10)
                {
                  tSpace = 0;
                  m_ofOut << " ";
                }
              c++;
            }
          m_ofOut << "\n";*/
        }
      b = 0;
      double dDelta;
      while(b < _s.m_vseqBest[a].m_vDomains.size())
        {
          m_outputStream->writeStartElement("domain");
          m_outputStream->writeAttribute("id",
                                         QString("%1.%2.%3")
                                           .arg(tId)
                                           .arg((unsigned long)(a + 1))
                                           .arg((unsigned long)(b + 1)));
          // m_ofOut << "<domain id=\"" << tId << "." << (unsigned long)(a + 1)
          //       << "." << (unsigned long)(b + 1) << "\" ";

          m_outputStream->writeAttribute(
            "start",
            QString::number(
              (unsigned long)(_s.m_vseqBest[a].m_vDomains[b].m_lS + 1)));
          //  m_ofOut << "start=\""
          //        << (unsigned long)(_s.m_vseqBest[a].m_vDomains[b].m_lS + 1)
          //   << "\" ";

          m_outputStream->writeAttribute(
            "end",
            QString::number(
              (unsigned long)(_s.m_vseqBest[a].m_vDomains[b].m_lE + 1)));
          // m_ofOut << "end=\""
          //        << (unsigned long)(_s.m_vseqBest[a].m_vDomains[b].m_lE + 1)
          //      << "\" ";

          m_outputStream->writeAttribute(
            "expect",
            evalueStr(_s.m_hHyper.expect(
              m_Score.hconvert(_s.m_vseqBest[a].m_vDomains[b].m_fHyper))));
          /* sprintf(pLine,
                   "%.1e",
                   _s.m_hHyper.expect(
                     m_Score.hconvert(_s.m_vseqBest[a].m_vDomains[b].m_fHyper)));*/
          // m_ofOut << "expect=\"" << pLine << "\" ";
          //  dDelta was subsituted for _s.m_vseqBest[a].m_fDelta in version
          //  2006.02.01 to improve the accuracy of the mass difference
          //  calculation
          m_outputStream->writeAttribute(
            "mh", massStr(_s.m_vseqBest[a].m_vDomains[b].m_dMH));

          dDelta = _s.m_dMH - _s.m_vseqBest[a].m_vDomains[b].m_dMH;
          /*
          if(fabs(dDelta) > 0.01)
            {
              sprintf(pLine, "%.3lf", _s.m_vseqBest[a].m_vDomains[b].m_dMH);
            }
          else
            {
              sprintf(pLine, "%.4lf", _s.m_vseqBest[a].m_vDomains[b].m_dMH);
            }
          m_ofOut << "mh=\"" << pLine << "\" ";*/


          m_outputStream->writeAttribute("delta", massStr(dDelta));
          /*
          if(fabs(dDelta) > 0.01)
            {
              sprintf(pLine, "%.3lf", dDelta);
            }
          else
            {
              sprintf(pLine, "%.4lf", dDelta);
            }
          m_ofOut << "delta=\"" << pLine << "\" ";*/

          m_outputStream->writeAttribute(
            "hyperscore",
            scoreStr(
              m_Score.hconvert(_s.m_vseqBest[a].m_vDomains[b].m_fHyper)));
          // m_Score.report_score(pLine,
          // _s.m_vseqBest[a].m_vDomains[b].m_fHyper); m_ofOut <<
          // "hyperscore=\"" << pLine << "\" ";
          m_outputStream->writeAttribute(
            "nextscore", scoreStr(m_Score.hconvert(_s.m_fHyperNext)));
          // m_Score.report_score(pLine, _s.m_fHyperNext);
          // m_ofOut << "nextscore=\"" << pLine << "\" ";

          m_outputStream->writeAttribute(
            "y_score",
            scoreStr(m_Score.hconvert(_s.m_mapScore[mscore::T_Y] +
                                      _s.m_mapScore[mscore::T_Z])));
          // m_Score.report_score(
          // pLine, _s.m_mapScore[mscore::T_Y] + _s.m_mapScore[mscore::T_Z]);
          // m_ofOut << "y_score=\"" << pLine << "\" ";
          m_outputStream->writeAttribute(
            "y_ions",
            QString::number(_s.m_mapCount[mscore::T_Y] +
                            _s.m_mapCount[mscore::T_Z]));
          // m_ofOut << "y_ions=\""
          //       << _s.m_mapCount[mscore::T_Y] + _s.m_mapCount[mscore::T_Z]
          //     << "\" ";
          m_outputStream->writeAttribute(
            "b_score",
            scoreStr(m_Score.hconvert(_s.m_mapScore[mscore::T_B] +
                                      _s.m_mapScore[mscore::T_C])));
          // m_Score.report_score(
          // pLine, _s.m_mapScore[mscore::T_B] + _s.m_mapScore[mscore::T_C]);
          // m_ofOut << "b_score=\"" << pLine << "\" ";

          m_outputStream->writeAttribute(
            "b_ions",
            QString::number(_s.m_mapCount[mscore::T_B] +
                            _s.m_mapCount[mscore::T_C]));
          // m_ofOut << "b_ions=\""
          //       << _s.m_mapCount[mscore::T_B] + _s.m_mapCount[mscore::T_C]
          //     << "\" ";
          m_outputStream->writeAttribute("pre",
                                         _s.m_vseqBest[a].getPre(b, 4).c_str());
          // m_ofOut << "pre=\"" << strTemp.c_str() << "\" ";
          m_outputStream->writeAttribute(
            "post", _s.m_vseqBest[a].getPost(b, 4).c_str());
          // m_ofOut << "post=\"" << strTemp.c_str() << "\" ";

          m_outputStream->writeAttribute(
            "seq", _s.m_vseqBest[a].getDomainSequence(b).c_str());
          /*
          m_ofOut << "seq=\""
                  << _s.m_vseqBest[a]
                       .m_strSeq
                       .substr(_s.m_vseqBest[a].m_vDomains[b].m_lS,
                               _s.m_vseqBest[a].m_vDomains[b].m_lE -
                                 _s.m_vseqBest[a].m_vDomains[b].m_lS + 1)
                       .c_str()
                  << "\" ";*/

          m_outputStream->writeAttribute(
            "missed_cleavages",
            QString::number(
              (int)_s.m_vseqBest[a].m_vDomains[b].m_lMissedCleaves));
          /*m_ofOut << "missed_cleavages=\""
                  << (int)_s.m_vseqBest[a].m_vDomains[b].m_lMissedCleaves
                  << "\">\n";*/
          c = 0;
          while(c < _s.m_vseqBest[a].m_vDomains[b].m_vAa.size())
            {

              m_outputStream->writeStartElement("aa");

              m_outputStream->writeAttribute(
                "type",
                QString(_s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_cRes));
              m_outputStream->writeAttribute(
                "at",
                QString::number(_s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_lPos +
                                1));
              /* m_ofOut << "<aa type=\""
                       << _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_cRes
                       << "\" at=\""
                       << _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_lPos + 1
                       << "\" ";*/

              m_outputStream->writeAttribute(
                "modified",
                massStr(_s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_dMod));
              // sprintf(
              // pLine, "%.5lf",
              // _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_dMod);
              // m_ofOut << "modified=\"" << pLine << "\" ";
              if(_s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_cMut != '\0' &&
                 _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_cMut !=
                   _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_cRes)
                {

                  m_outputStream->writeAttribute(
                    "pm",
                    QString(_s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_cMut));
                  // m_ofOut << "pm=\""
                  //       << _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_cMut
                  //     << "\" ";
                }
              if(_s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_strId.size() != 0)
                {
                  m_outputStream->writeAttribute(
                    "id",
                    _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_strId.c_str());
                  // m_ofOut
                  //<< "id=\""
                  //<< _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_strId.c_str()
                  //<< "\" ";
                }
              if(fabs(_s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_dPrompt) > 0.1)
                {

                  m_outputStream->writeAttribute(
                    "prompt",
                    QString::number(
                      _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_dPrompt));
                  // sprintf(pLine,
                  //       "%.5lf",
                  //     _s.m_vseqBest[a].m_vDomains[b].m_vAa[c].m_dPrompt);
                  // m_ofOut << "prompt=\"" << pLine << "\" ";
                }

              m_outputStream->writeEndElement(); //("aa");
                                                 //  m_ofOut << "/>\n";
              c++;
            }

          m_outputStream->writeEndElement(); //("domain");
          // m_ofOut << "</domain>\n";
          b++;
        }

      m_outputStream->writeEndElement(); //("peptide");

      m_outputStream->writeEndElement(); //("protein");
      // m_ofOut << "</peptide>\n</protein>\n";
      a++;
    }
  // delete pLine;
  return true;
}

/*
 * start is called first to set up the XML header
 */
bool
mreport::start(XmlParameter &_x)
{
  string strKey = "output, path";
  string strValue;
  _x.get(strKey, strValue);
  if(strValue.size() == 0)
    return false;
  string strPath = strValue;
  strKey         = "output, path hashing";
  _x.get(strKey, strValue);
  if(strValue == "yes")
    {
      size_t tStart = strPath.rfind('.');
      if(tStart != strPath.npos)
        {
          tStart++;
          time_t tValue;
          time(&tValue);
          char pLine[256];
          struct tm *ptmValue = localtime(&tValue);
          if(ptmValue == NULL)
            {
              ptmValue = gmtime(&tValue);
            }
          if(ptmValue != NULL)
            {
              strftime(pLine, 255, "%Y_%m_%d_%H_%M_%S.t.", ptmValue);
            }
          else
            {
              srand((unsigned)time(NULL));
              sprintf(pLine, "%i.t", rand());
            }
          strPath.insert(tStart, pLine);
        }
    }


  m_outputFile = new QFile(strPath.c_str());

  if(m_outputFile->open(QIODevice::WriteOnly))
    {
      m_outputStream = new QXmlStreamWriter();
      m_outputStream->setDevice(m_outputFile);
    }
  else
    {
      throw pappso::PappsoException(
        QString(
          "ERROR writing XML output to file %1, check path or file permission")
          .arg(strPath.c_str()));
      // return false;
    }

  m_outputStream->setAutoFormatting(true);
  m_outputStream->writeStartDocument("1.0");

  strKey = "output, title";
  _x.get(strKey, strValue);
  m_outputStream->writeStartElement("bioml");

  // xmlns:GAML="http://www.bioml.com/gaml/"
  m_outputStream->writeNamespace(m_xmlnsGaml, "GAML");
  if(strValue.size() > 0)
    {
      // m_ofOut << "<bioml xmlns:GAML=\"http://www.bioml.com/gaml/\" ";
      m_outputStream->writeAttribute("label", strValue.c_str());
      // m_ofOut << "label=\"" << strValue.c_str() << "\">\n";
    }
  else
    {
      strKey = "spectrum, path";
      _x.get(strKey, strValue);
      // m_ofOut << "<bioml xmlns:GAML=\"http://www.bioml.com/gaml/\" ";

      m_outputStream->writeAttribute(
        "label", QString("models from '%1'").arg(strValue.c_str()));
      // m_ofOut << "label=\"models from '" << strValue.c_str() << "'\">\n";
    }
  return true;
}

QString
mreport::massStr(double mass)
{
  return QString::number(mass, 'f', 10);
}

QString
mreport::scoreStr(float score)
{
  return QString::number(score, 'f', 1);
}


QString
mreport::evalueStr(double score)
{
  return QString::number(score, 'g', 10);
}

QString
mreport::label80Str(const QString &label) const
{
  return label.mid(0, 80);
}
