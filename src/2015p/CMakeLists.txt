# CMake script to build tandemng (X!Tandem fork)
# Author: Olivier Langella
# Created: 24/02/2024


#configure_file(${CMAKE_SOURCE_DIR}/src/config.h.cmake ${CMAKE_SOURCE_DIR}/src/config.h)

# File list


file(GLOB CPP_FILES ${CMAKE_SOURCE_DIR}/src/msequence.cpp ${CMAKE_SOURCE_DIR}/src/masscalc.cpp ${CMAKE_SOURCE_DIR}/src/xmlparameter.cpp ${CMAKE_SOURCE_DIR}/src/msequtilities.cpp ${CMAKE_SOURCE_DIR}/src/mreport.cpp ${CMAKE_SOURCE_DIR}/src/lib/*.cpp ${CMAKE_SOURCE_DIR}/src/mzid_report.cpp ${CMAKE_SOURCE_DIR}/src/2015p/*.cpp)

#message("i2MassChroQ_SRCS:  ${i2MassChroQ_SRCS}")


if(UNIX AND NOT APPLE)

  add_executable(tandemng2015p ${CPP_FILES})

elseif(APPLE)

elseif(MXE)

  add_executable(tandemng2015p ${CPP_FILES})
  target_compile_options(tandemng2015p PRIVATE -mwindows)
  set_target_properties(tandemng2015p PROPERTIES LINK_FLAGS "-Wl,--subsystem,windows ${CMAKE_EXE_LINKER_FLAGS}")

endif()

target_compile_definitions(tandemng2015p PUBLIC HEADER2015P)

#-lpthread -L/usr/lib -lm -lexpat
target_link_libraries(tandemng2015p
  Threads::Threads
  EXPAT::EXPAT
  Qt6::Core
  Qt6::Xml
  Qt6::Sql
  PappsoMSpp::Core
)




install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/tandemng2015p DESTINATION bin)

