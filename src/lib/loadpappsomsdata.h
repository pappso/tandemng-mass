/**
 * \file src/lib/loadpappsomsdata.h
 * \date 11/01/2024
 * \author Olivier Langella
 * \brief reads MS data file using the PAPPSOms++ library
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of tandemng.
 *
 *     this file is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this file.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/msfile/msfileaccessor.h>


#ifdef HEADER2017
#include "../mspectrum.h"
#include "../mspectrumcondition.h"
#include "../mscore.h"
#endif


#ifdef HEADER2015
#include "../2015/mspectrum.h"
#include "../2015/mspectrumcondition.h"
#include "../2015/mscore.h"
#endif


#ifdef HEADER2015P
#include "../2015p/mspectrum.h"
#include "../2015p/mspectrumcondition.h"
#include "../2015p/mscore.h"
#endif

QString mspectrumToString(const mspectrum &tandem_spectrum);
/**
 * @todo write docs
 */
class LoadPappsoMsData : public pappso::SpectrumCollectionHandlerInterface
{
  public:
  virtual ~LoadPappsoMsData();
  virtual void setQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &qualified_mass_spectrum) override;
  virtual bool needPeakList() const override;
  virtual void spectrumListHasSize(std::size_t size) override;

  const std::vector<pappso::QualifiedMassSpectrum> &
  getQualifiedMassSpectrumList() const;

  // stores process performance parameters
  // store process input parameterspappso::MsFileAccessor *p_file_accessor,
  static pappso::MsRunReaderSPtr
  getMsRunReaderSPtr(XmlParameter &xml_performance,
                     XmlParameter &xml_values,
                     pappso::MsFileAccessor *p_file_accessor,
                     const QString &filename);

  static bool readAllMs2Spectrum(const pappso::MsRunReaderSPtr &msrun_reader,
                                 vector<mspectrum> &vSpectra,
                                 mspectrumcondition &m_specCondition,
                                 mscore *m_pScore);

  enum
  {
    I_Y = 0x01,
    I_B = 0x02,
    I_X = 0x04,
    I_A = 0x08,
    I_C = 0x10,
    I_Z = 0x20,
  } ion_type; // enum for referencing information about specific ion types.

  private:
  LoadPappsoMsData(vector<mspectrum> &vSpectra,
                   mspectrumcondition &m_specCondition,
                   mscore *m_pScore);

  private:
  vector<mspectrum> &m_vSpectra;
  mspectrumcondition
    m_specCondition; // the mspectrumcondition object that cleans up and
                     // normalized spectra for further processing
  mscore *m_pScore;  // the object that is used to score sequences and spectra
};
