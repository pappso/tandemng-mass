/**
 * \file src/lib/tandeminputparser.cpp
 * \date 13/06/2024
 * \author Olivier Langella
 * \brief reads tandem input file
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of tandemng.
 *
 *     this file is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this file.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "tandeminputparser.h"
#include <QDebug>

TandemInputParser::TandemInputParser(xMap &_map)
{
  m_mapParam = &_map;
}

TandemInputParser::TandemInputParser(const TandemInputParser &other)
{
}

TandemInputParser::~TandemInputParser()
{
}

void
TandemInputParser::readStream()
{
  qDebug();
  if(m_qxmlStreamReader.readNextStartElement())
    {
      if(m_qxmlStreamReader.name().toString() == "bioml")
        {
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();
              if(m_qxmlStreamReader.name().toString() == "note")
                {

                  QString type =
                    m_qxmlStreamReader.attributes().value("type").toString();
                  QString label =
                    m_qxmlStreamReader.attributes().value("label").toString();

                  qDebug() << m_qxmlStreamReader.name() << " type=" << type
                           << " label=" << label;

                  if(label.isEmpty())
                    {
                      m_qxmlStreamReader.skipCurrentElement();
                    }
                  else
                    {

                      // if(isElement("note", el) &&
                      // !(strcmp("input", getAttrValue("type",
                      // attr)))){ m_strKey = getAttrValue("label",
                      // attr);
                      (*(m_mapParam))[label.toStdString()] =
                        m_qxmlStreamReader.readElementText().toStdString();

                      qDebug() << (*m_mapParam)[label.toStdString()].c_str();
                    }
                }
              else
                { // note
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("Not an X!Tandem input file (no note)"));
                  m_qxmlStreamReader.skipCurrentElement();
                }
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("Not an X!Tandem input file (no bioml)"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }

  qDebug();
}
