/**
 * \file src/lib/tandeminputparser.cpp
 * \date 13/06/2024
 * \author Olivier Langella
 * \brief reads tandem input file
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of tandemng.
 *
 *     this file is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this file.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QFile>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "../xmlparameter.h"

#include "../stdafx.h"

/**
 * @todo write docs
 */
class TandemInputParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  TandemInputParser(xMap &_map);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TandemInputParser(const TandemInputParser &other);

  /**
   * Destructor
   */
  ~TandemInputParser();

  protected:
  virtual void readStream() override;


  private:
  private:
  xMap *m_mapParam;
};
