[Setup]
AppName=${SOFTWARE_NAME}

#define public winerootdir "z:"
; Set version number below
#define public version "${VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "z:/home/langella/developpement/git/packaging/tandemng/"
#define cmake_build_dir "z:/home/langella/developpement/git/packaging/tandemng/wbuild"

; Set version number below
AppVerName=${SOFTWARE_NAME} version {#version}
DefaultDirName={pf}\tandemng
DefaultGroupName=tandemng
OutputDir="{#cmake_build_dir}"

; Set version number below
OutputBaseFilename=tandemng-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=tandemng-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\LICENSE"
AppCopyright="Copyright (C) Artistic license Ron Beavis original code modified in 2024- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="tandemng, formerly X!Tandem"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "z:/win64/mxeqt6_dll/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/qt6/plugins/*"; DestDir: {app}/plugins; Flags: ignoreversion recursesubdirs
Source: "z:/home/langella/developpement/git/cutelee/wbuild/templates/lib/libCuteleeQt6Templates.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/cutelee/wbuild/cutelee-qt6/6.1/*"; DestDir: {app}/cutelee-qt6/6.1; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libpwizlite/wbuild/src/libpwizlite.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: tandemngComp
Source: "{#sourceDir}\LICENSE"; DestDir: {app}; Flags: isreadme; Components: tandemngComp
; Source: "{#sourceDir}\win64\xtandempipeline_icon.ico"; DestDir: {app}; Components: tandemngComp

Source: "{#cmake_build_dir}\src\tandemng.exe"; DestDir: {app}; Components: tandemngComp
Source: "{#cmake_build_dir}\src\2015\tandemng2015.exe"; DestDir: {app}; Components: tandemngComp
Source: "{#cmake_build_dir}\src\2015p\tandemng2015p.exe"; DestDir: {app}; Components: tandemngComp

[Icons]
Name: "{group}\tandemng"; Filename: "{app}\tandemng.exe"; WorkingDir: "{app}";IconFilename: "{app}\xtandempipeline_icon.ico"
Name: "{group}\Uninstall tandemng"; Filename: "{uninstallexe}"

[Types]
Name: "tandemngType"; Description: "Full installation"

[Components]
Name: "tandemngComp"; Description: "tandemng files"; Types: tandemngType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\tandemng.exe"; Description: "Launch tandemng"; Flags: postinstall nowait unchecked

