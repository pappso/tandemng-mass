message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")
message("If building of the user manual is required, add -DBUILD_USER_MANUAL=1")
message("If using the locally built pappsomspp libs, add -DLOCAL_DEV=1")

# The TARGET changes according to the plaform
# For example, it changes to i2MassChroQ for macOS.
# Here we want it to be lowercase, UNIX-culture,
# and it is needed for the user manual build.
SET(TARGET tandemng)
SET(CAP_TARGET tandemng)

## Install directories
if(NOT CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX /usr)
endif()
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})


find_package(Threads REQUIRED)

find_package(EXPAT REQUIRED)

find_package( Qt6 COMPONENTS Core Xml Sql REQUIRED )





set(PappsoMSpp_INCLUDE_DIR /home/langella/developpement/git/pappsomspp/src)
mark_as_advanced(PappsoMSpp_INCLUDE_DIR)
set(PappsoMSpp_INCLUDE_DIRS ${PappsoMSpp_INCLUDE_DIR})
# Look for the necessary library
set(PappsoMSpp_LIBRARY /home/langella/developpement/git/pappsomspp/build/src/libpappsomspp.so)
mark_as_advanced(PappsoMSpp_LIBRARY)
# Mark the lib as found
set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_LIBRARIES ${PappsoMSpp_LIBRARY})
if(NOT TARGET PappsoMSpp::Core)
    add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Core PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSpp_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()
set(PappsoMSppWidget_LIBRARY /home/langella/developpement/git/pappsomspp/build/src/pappsomspp/widget/libpappsomspp-widget.so)
mark_as_advanced(PappsoMSppWidget_LIBRARY)  
message(STATUS "~~~~~~~~~~~~~ ${PappsoMSppWidget_LIBRARY} ~~~~~~~~~~~~~~~")
set(PappsoMSppWidget_FOUND TRUE)
if(NOT TARGET PappsoMSpp::Widget)
    add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Widget PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSppWidget_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()

