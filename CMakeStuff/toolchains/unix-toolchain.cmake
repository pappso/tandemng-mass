message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")
message("If building of the user manual is required, add -DBUILD_USER_MANUAL=1")
message("If using the locally built pappsomspp libs, add -DLOCAL_DEV=1")

# The TARGET changes according to the plaform
# For example, it changes to i2MassChroQ for macOS.
# Here we want it to be lowercase, UNIX-culture,
# and it is needed for the user manual build.
SET(TARGET tandemng)

## Install directories
if(NOT CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX /usr)
endif()
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})

find_package(Threads REQUIRED)

find_package(EXPAT REQUIRED)

find_package( Qt6 COMPONENTS Core Xml Sql REQUIRED )

find_package(PappsoMSpp COMPONENTS Core REQUIRED)
